import router from './index'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style

router.beforeEach((to, from, next) => {
  next()
  console.log(to)
})

router.afterEach(() => {
  NProgress.done() // 结束Progress
})
