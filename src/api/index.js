import { Request } from 'taro-toolset'
import { getToken } from '@/utils/auth'
import { Toast } from 'vant'
import router from '@/router'
import store from '@/store'

function redirectToLogin() {
  store.dispatch('user/resetToken').then(() => {
    router.push('/login')
    Toast.fail('登录超时，请重新登录')
  })
}

export function generateBaseUrl(pageURL, uri) {
  console.log(uri)
  // let baseUrl =
  // 'http://192.168.10.16:8092/api/apimock-v2/a647d470ba213416f188d78b6dff3a8a'
  // let baseUrl = 'http://172.16.176.56:8082' // 高健本地
  // let baseUrl = 'http://172.16.144.224:8082' // 言行本地
  // let baseUrl = 'http://172.16.176.106:9527' // 志鑫本地
  // let baseUrl = 'http://172.16.147.213:8888' //光宏本地
  let baseUrl = 'http://192.168.10.16:9100' // 测试服务
  // let baseUrl = 'http://172.16.176.241:8888' // 文杰本地
  if (pageURL.includes('192.168.10.16')) {
    baseUrl = 'http://192.168.10.16:9100'
  }

  // 生产环境域名配置
  if (pageURL.includes('meishangcorp.com')) {
    baseUrl = 'https://wgapi.meishangcorp.com'
  }
  return baseUrl
}

const service = new Request({
  timeout: 1000 * 60,
  successCode: (code) => [0, '0'].includes(code),
  partBase: generateBaseUrl,
  partHeaders(uri) {
    console.log(uri)
    return {
      Authorization: getToken() ? `Bearer ${getToken()}` : '',
    }
  },
  ignoreAnyCatch: [],
  handleResponseError: {
    codeIgnore: [100],
    handler(code, message) {
      if (code === 401004 || code === 401002) {
        redirectToLogin()
      } else {
        Toast.fail(message)
      }
    },
  },
  handleAccessDenied: {
    statusCode: [401, 403],
    handler(code, message) {
      if (code === 401) {
        redirectToLogin()
      } else {
        Toast.fail(message)
      }
    },
  },
})

export default service.getInstance()
