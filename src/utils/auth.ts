import Cookies from 'js-cookie'
const USER_TOKEN_KEY = 'user-a-client-token'
const USER_LOGIN_PHONE_KEY = 'user-login-phone'

export function getToken(): string | undefined {
  return Cookies.get(USER_TOKEN_KEY)
}

export function setToken(token: string): string | undefined {
  return Cookies.set(USER_TOKEN_KEY, token)
}

export function removeToken(): void {
  return Cookies.remove(USER_TOKEN_KEY)
}

export function getLoginPhone(): string | undefined {
  return Cookies.get(USER_LOGIN_PHONE_KEY)
}

export function setLoginPhone(phone: string): string | undefined {
  return Cookies.set(USER_LOGIN_PHONE_KEY, phone)
}
